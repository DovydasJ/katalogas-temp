<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'categories' => 'Categories',
    'cart' => 'Cart',
    'search' => 'Search',
    'product' => 'Product',
    'shopping.cart' => 'Shopping Cart',
    'home' => 'home',
    'catalog' => 'Catalogue',
    'addToCart' => 'Add to Cart',
];

