<?php

use App\Models\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductControllerTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_finds_products()
    {
        factory(Product::class)->create(['name' => 'AA']);
        factory(Product::class)->create(['name' => 'BBA']);
        factory(Product::class)->create(['name' => 'CC']);

        $this->get(url('product/search?name=A'))->seeStatusCode(200);
        $data = $this->response->getOriginalContent()->getData();
        $viewProductCount = $data['products']->getCollection()->count();

        $this->assertEquals(2, $viewProductCount);
    }
}