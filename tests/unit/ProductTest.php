<?php

use App\Models\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
class ProductTest extends TestCase {

    use DatabaseTransactions;

    /** @test */
    public function it_fetches_products()
    {
        //Given
        factory(Product::class,3)->create(['name' => 'TestObject']);
        factory(Product::class,2)->create(['name' => 'TestObject2']);

        //When

        $products = Product::all();

        //Then

        $this->assertEquals($products->where('name', 'TestObject')->count(), 3);
        $this->assertEquals($products->where('name', 'TestObject2')->count(), 2);
    }

    /** @test */
    public function it_sorts_products()
    {
        //Given
        $first = factory(Product::class)->create(['name' => 'A']);
        $last = factory(Product::class)->create(['name' => 'Z']);

        //When

        $products = Product::sorted();

        //Then

        $this->assertEquals($first->name,$products->first()->name);
        $this->assertEquals($last->name,$products->last()->name);
    }

}