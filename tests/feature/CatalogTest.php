<?php

use App\Models\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CatalogTest extends TestCase
{

    use DatabaseTransactions;

    /** @test */
    public function it_displays_products_catalog()
    {
        $response = $this->get(route('product.all'));
        $response->assertResponseStatus(200);
    }

    /** @test */
    public function it_displays_single_product_page()
    {
        $product = factory(Product::class)->create(['name' => 'AA']);
        $response = $this->get(route('product.single', ['id' => $product->getKey()]));
        $response->assertResponseStatus(200)->see('AA');
    }


}