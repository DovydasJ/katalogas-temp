<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';

    
    public function scopeSorted()
    {
        return $this->all()->sortBy("name",SORT_NATURAL);
    }
}