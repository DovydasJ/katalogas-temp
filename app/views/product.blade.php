
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/item.css')}}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
@extends('layouts.navigation')

@section('content')
    <div class="container-fluid">
        <div class="modal-body row">
            <div class="col-md-6">
                <figure class="figure">
                    @if ($product->image_url == null)
                        <img src="https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg"
                             alt="...">
                    @else
                        <img src="{{$product->image_url}}" alt="...">
                    @endif


                    <figcaption class="figure-caption text-left">
                        <h3>{{ $product->name }}
                            <small>Price: {{ $product->price }} €</small>

                        </h3>
                        <p class="description">{{ $product->description }}</p>
                        <a href="{{ route('cart.add',['id' => $product->id]) }}"
                           class="btn btn-sucess pull-left" role="button">@lang('main.addToCart')</a>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Specifications</div>
                    <div class="panel-body">
                        <p>...</p>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">Whatever</li>
                        <li class="list-group-item">Dapibus ac facilisis in</li>
                        <li class="list-group-item">Morbi leo risus</li>
                        <li class="list-group-item">Porta ac consectetur ac</li>
                        <li class="list-group-item">Vestibulum at eros</li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
@endsection