<!DOCTYPE html>
<html lang="en">
<head>
    <title>Katalogas</title>
    <script  src="{{ URL::asset('js/app.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css')}}">


</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">@lang('main.catalog')</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">@lang('main.categories') <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route("product.all")}}">All</a></li>
                            @foreach($categories as $category)
                            <li><a href="{{route('product.category',['category' => $category->id])}}">{{$category->name}}</a></li>
                                @endforeach
                        </ul>
                    </li>
                </ul>

                <form class="navbar-form navbar-left" method="GET" action="{{route('product.search',['name'])}}" >
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="@lang('main.product')">
                    </div>
                    <button type="submit"  class="btn btn-default">@lang('main.search')</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{route('cart.view')}}">
                        <i class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i> @lang('main.cart')
                        <span class="badge">{{Session::has('cart') ? Session::get('cart')->totalQuantity : ''}}</span>
                        </a>
                    </li>
                </ul>
                <a href="{{ route('lang.switch', 'lt') }}">
                <img class="navbar-brand navbar-right" src="https://cdn1.iconfinder.com/data/icons/european-country-flags/83/lithuania-128.png" alt="" style="width:70px;height:70px;margin:-10px">
                </a>
                <a href="{{ route('lang.switch', 'en') }}">
                    <img class="navbar-brand navbar-right" src="https://cdn2.iconfinder.com/data/icons/world-flag-icons/128/Flag_of_United_Kingdom.png" alt="" style="width:70px;height:70px;margin:-10px">
                </a>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    @yield('content')
</div>
</body>
</html>