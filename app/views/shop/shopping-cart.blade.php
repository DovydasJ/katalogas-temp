<!-- CSS And JavaScript -->

<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/cart.css')}}">
@extends('layouts.navigation')

@section('content')
    <section id="cart_items">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="/">@lang('main.home')</a></li>
                <li class="active">@lang('main.shopping.cart')</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            @if(isset($products))
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Total Price</td>
                        <td class="quantity">Quantity</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td class="cart_product">
                                @if ($product['item']['image_url'] === null)
                                    <a href="{{ route('product.single',['id' => $product['item']['id']])}}">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg"
                                         alt="..." class="img-responsive">
                                    </a>
                                @else
                                        <a href="{{ route('product.single',['id' => $product['item']['id']]) }}"><img src="{{$product['item']['image_url']}}" alt="..."></a>
                                @endif
                            </td>
                            <td class="cart_product_name">
                                <h4><a href="{{ route('product.single',['id' => $product['item']['id']]) }}">{{$product['item']['name']}}</a></h4>
                            </td>
                            <td class="cart_price">
                                <p>${{number_format($product['price'],2)}}</p>
                            </td>
                            <td class="cart_quantity">
                                {{$product['quantity']}}
                                <a href="{{ route('cart.increase',['id' => $product['item']['id']]) }}"
                                   class="btn btn-sucess" role="button"><span class="badge">+</span></a>
                                <a href="{{ route('cart.reduce',['id' => $product['item']['id']]) }}"
                                   class="btn btn-sucess" role="button"><span class="badge">-</span></a>
                            </td>

                            <td class="cart_delete">
                                <a class="btn btn-default"
                                   href='{{ route('cart.removeItem',['id' => $product['item']['id']]) }}'> Remove
                                    Item</a>
                            </td>
                        </tr>
                    @endforeach



                    </tbody>
                </table>
        </div>
        <div class="pull-right">
            <strong>Total Price:</strong>
            <span class="badge">{{$totalPrice}} $</span>
        </div>
        <a class="btn btn-default" href={{route('cart.clear')}}>Clear cart</a>

        @if(isset($token))
                <div class="input-group">
                    <span class="input-group-addon">Share your cart</span>
                    <input id="msg" type="text" class="form-control" value='{{url("/shopping-cart?token=$token")}}' name="msg"
                           placeholder="Additional Info" readonly>
                </div>
            @else <a class="btn btn-default" href='{{route("cart.save")}}'>Save cart</a>
        @endif


    </section>


    @else
        <p>You have no items in the shopping cart</p>
    @endif
@endsection