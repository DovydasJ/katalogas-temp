<?php
/**
 * Created by PhpStorm.
 * User: Dovydas
 * Date: 2017-02-06
 * Time: 15:27
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class EditCartItem extends FormRequest
{
    public function __construct(ValidationFactory $validationFactory)
    {
        $validationFactory->extend(
            'item_exists',
            function ($attribute, $value, $parameters) {

                if (Session::has('cart')) {
                    $cart = Session::get('cart');
                }
                if (array_has($cart->items, $value)) {
                    return 'item_exists' === $value;
                }
            },
            'Sorry, it failed foo validation!'
        );

    }

    public function rules()
    {
        return [
            'id' => 'item_exists',
        ];
    }

}