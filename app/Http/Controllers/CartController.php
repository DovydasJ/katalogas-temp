<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;

class CartController extends Controller
{

    /**
     * Shows cart session
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCart()
    {
        if (Input::get('token') !== null) {
            $token = Input::get('token');
            $cart = $this->getSavedCart($token);
            Session::put('cart', $cart);
            return $this->renderCartView($cart, $token);
        }

        if (!Session::has('cart')) {
            return $this->renderCartView();
        }

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $token = Input::get('token');
        return $this->renderCartView($cart, $token);
    }

    /**
     * Add item to cart session
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \RuntimeException
     */
    public function addToCart(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }

    /**
     * Reduces item count in current cart session
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reduceItemByOne($id)
    {
        if (!Session::has('cart')) {
            return $this->renderCartView();
        }

        $cart = Session::get('cart');

        if (array_key_exists($id, $cart->items)) {
            $cart->reduceByOne($id);
            Session::put('cart', $cart);
        }

        if ($cart->totalQuantity <= 0) {
            $this->clear();
        }

        return redirect()->route('cart.view');
    }

    /**
     * Increases item count in current cart session
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function increaseItemByOne($id)
    {
        if (!Session::has('cart')) {
            return $this->renderCartView();
        }

        $cart = Session::get('cart');
        if (array_key_exists($id, $cart->items)) {
            $cart->increaseByOne($id);
            Session::put('cart', $cart);
        }

        return redirect()->route('cart.view');
    }

    /**
     * Removes cart from current session
     * 
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function removeItem($id)
    {
        if (!Session::has('cart')) {
            return $this->renderCartView();
        }

        $cart = Session::get('cart');
        if (array_key_exists($id, $cart->items)) {
            $cart->removeItem($id);
        }

        if ($cart->totalQuantity === 0) {
            Session::forget('cart');
        } else {
            Session::put('cart', $cart);
        }

        return redirect()->route('cart.view');
    }

    /**
     * Clears current cart from the session
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function clear()
    {
        if (!Session::has('cart')) {
            return $this->renderCartView();
        }

        Session::forget('cart');
        return $this->renderCartView();
    }

    /**
     * Saves the cart to database
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save()
    {
        if (!Session::has('cart')) {
            return $this->renderCartView();
        }

        $order = new Order();
        $cart = Session::get('cart');
        while (true) {
            $randomToken = str_random(16);
            if (Order::where('token', $randomToken)->count() > 1) {
                $randomToken = str_random(16);
            } else {
                break;
            }
        }

        $order->cart = serialize($cart);
        $order->token = $randomToken;
        $order->save();

        return redirect()->route('cart.sharedView', [$randomToken]);
    }

    /**
     * Returns saved cart from database
     * 
     * @param string $token
     *
     * @return mixed
     */
    private function getSavedCart($token)
    {
        $order = Order::where('token', $token)->get();
        $cart = unserialize($order->first()->cart);
        return $cart;
    }
    /**
     * Renders cart view
     *
     * @param null $cart
     * @param null $token
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function renderCartView($cart = null, $token = null)
    {
        if ($cart !== null) {
            return view('shop.shopping-cart', [
                'products' => $cart->items,
                'token' => $token,
                'totalPrice' => number_format($cart->totalPrice, 2),
            ]);
        }

        return view('shop.shopping-cart');
    }
}
