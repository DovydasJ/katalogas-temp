<?php
namespace App\Http\Controllers;

use App\Http\Requests\SearchField;
use App\Models\CartProduct;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Session;

class ProductController extends Controller
{
    /**
     * Returns and renders all products sorted by name
     * 
     * @return View
     */
    public function getProducts()
    {
        $products = Product::orderBy('name')->paginate(5);
        return $this->renderMarketplaceView($products);
    }

    /**
     * Returns and renders product by id
     * 
     * @param int $id
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function getProduct($id)
    {
        $product = Product::find($id);
        return view('product', ['product' => $product]);
    }

    /**
     * Returns and renders products by category
     * 
     * @param Category $category
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function getCategoryProducts(Category $category = null)
    {
        $products = $category->products()->orderBy('name')->paginate(5);
        return $this->renderMarketplaceView($products);
    }
    /**
     * Searches for product in database
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function search()
    {
        $name = Input::get('name');
        $products = Product::where('name', 'LIKE', '%' . $name . '%')->paginate(15);
        return $this->renderMarketplaceView($products);
    }

    /**
     * Renders marketplace view
     * 
     * @param Product $products
     *
     * @return \Illuminate\Support\Facades\View
     */
    private function renderMarketplaceView($products)
    {
        return view('marketplace', ['products' => $products]);
    }
}
